#include <iostream>
#include <string>
#include <vector>
#include "item.h"

class Character
{
private:
    std::string name;
    int health;
    std::vector<Item> inventory;

public:
    Character(const std::string& n, int hp) {
        name = n;
        health = hp;
    }

    int getHealth() {
        return health;
    }
};


class Player : public Character
{
private:
    Room *location;

public:
    //Player(const std::string &name, int health);
    Player(std::string pName, int pHealth) : Character(pName, pHealth), location(nullptr) {};

    void SetLocation(Room *set)
    {
        location = set;
    }

    Room *GetLocation()
    {
        return location;
    }
};