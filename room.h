#include <iostream>
#include <algorithm>
#include <string>
#include <map>
#include <vector>
#include "item.h"
using namespace std;

class Room
{
private:
    std::string description;
    std::map<std::string, Room *> exits;
    std::vector<Item> items;
    int roomID;

public:
    Room(const std::string &desc)
    {
        description = desc;
    }

    void AddItem(const Item &item)
    {
        items.push_back(item);
    }

    void RemoveItem(const Item &item)
    {
        auto it = std::find(items.begin(), items.end(), item);
        items.erase(items.begin() + distance(items.begin(), it));
    }

    void AddExit(const std::string &direction, Room *room)
    {
        exits[direction] = room;
    }

    std::string GetDescription()
    {
        return description;
    }

    std::vector<Item> &GetItems()
    {
        return items;
    }

    Room *GetExit(const std::string &direction)
    {
        return exits[direction];
    }

    bool isValidExit(const std::string &direction)
    {
        return exits.find(direction) != exits.end();
    }

    int GetRoomID()
    {
        return roomID;
    }
};
