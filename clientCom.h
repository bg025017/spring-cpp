#include <iostream>
#include <string>
#include <curl/curl.h>
#include <algorithm>
//sudo apt-get install libcurl4-openssl-dev
//remmeber to link the library with -lcurl

#pragma once
class ClientCom
{
private:
    std::string player;

    // Initialize CURL
    CURL *curl;
    CURLcode res;

    static size_t WriteCallback(void *contents, size_t size, size_t nmemb, std::string *s)
    {
        size_t newLength = size*nmemb;
        size_t oldLength = s->size();
        try
        {
            s->resize(oldLength + newLength);
        }
        catch(std::bad_alloc &e)
        {
            //handle memory problem
            return 0;
        }

        std::copy((char*)contents,(char*)contents+newLength,s->begin()+oldLength);
        return size*nmemb;
    }

    std::string sendRequest(const std::string &requestType, const std::string &query = "")
    {
        std::string readBuffer;
        curl = curl_easy_init();
        if(curl) {
            std::string url = "http://localhost:8080/" + requestType;
            if (!query.empty()) {
                url += "?" + query;
            }
            //url encode the query
            std::cout << "|| url: " + url << std::endl;
            curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
            res = curl_easy_perform(curl);
            if(res != CURLE_OK)
                fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(res));
            curl_easy_cleanup(curl);
        }
        std::cout << "|| server res: " + readBuffer << std::endl;
        return readBuffer;
    }

    std::string iHateCppIWantNodeBackReplaceSpaceWithPlus(std::string text)
        {
            std::replace(text.begin(), text.end(), ' ', '+');
            return text;
        }




public:
ClientCom()
{
    player = "";
    curl = nullptr;
}

    void login() {
        std::string res = sendRequest("playerAssign");
        std::string message = "you are player " + res;
        std::cout << message << std::endl;
        player = res;
    }

    std::string getPlayer() {
        return player;
    }

    bool requestStart() {
        std::string res = sendRequest("start");
        std::cout << "sent request to start" << std::endl;
        std::cout << res << std::endl;
        if(res == "start") {
            return true;
        }
        return false;
    }

    std::string sendLocation(const std::string &location, int score) {

        std::string query = "player=" + player + "&location=" + iHateCppIWantNodeBackReplaceSpaceWithPlus(location) + "&score=" + std::to_string(score);
        std::cout << "query: " + query << std::endl;
        std::string res = sendRequest("playerLocation", query); //either message or "safe"
        return res;
    }
};