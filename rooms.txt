Room|corridor|You are in a lengthy corridor.
Room|startingroom|You step into the castle's entrance.
Room|vault|You enter a room brimming with jewels and wealth.
Room|library|You enter a room filled with ancient books.
Room|dungeon|You enter a dark and damp dungeon.
Connection|corridor|vault|north
Connection|corridor|startingroom|south
Connection|corridor|library|east
Connection|corridor|dungeon|west
Item|startingroom|Potion|A special potion that has a unknown effect.
Item|startingroom|Potion|A special potion that has a unknown effect.
Item|corridor|Potion|A special potion that has a unknown effect.
Item|library|Book|An ancient book with mysterious symbols.
Item|dungeon|Torch|A torch that barely lights up the room.
Connection|startingroom|corridor|north
Connection|vault|corridor|south
Connection|library|corridor|west
Connection|dungeon|corridor|east
Item|vault|Sword|A special sword found in the treasury.
Item|vault|Potion|A special potion that has a unknown effect.
Item|library|Scroll|A scroll with a powerful spell written on it.
Item|dungeon|Key|A rusty key that might open a locked door.
