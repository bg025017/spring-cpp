#ifndef AREA_H
#define AREA_H

#include <iostream>
#include <map>
#include <fstream>
#include <sstream>
#include <vector>

class Area {

    private:
        std::map<std::string, Room*> rooms;
    
    public:

        Area(void) {};

        void AddRoom(const std::string& name, Room* room) {
            if (room != nullptr) {
                rooms.insert({name,room});
            }
        }

        Room* getRoom(const std::string& target) {
            for (auto it = rooms.begin(); it != rooms.end(); ++it) {
                if (it->first == target) {
                    return it->second;
                }
            }
            return nullptr;
        }

        void connectRooms(const std::string& room1, const std::string& room2, const std::string& target) {
                if (target == "north") {
                    getRoom(room1)->AddExit(target, getRoom(room2));
                    getRoom(room2)->AddExit(oppositeD(target), getRoom(room1));
                } else if (target == "south") {
                    getRoom(room1)->AddExit(target, getRoom(room2));
                    getRoom(room2)->AddExit(oppositeD(target), getRoom(room1));
                } else if (target == "east") {
                    getRoom(room1)->AddExit(target, getRoom(room2));
                    getRoom(room2)->AddExit(oppositeD(target), getRoom(room1));
                } else if (target == "west") {
                    getRoom(room1)->AddExit(target, getRoom(room2));
                    getRoom(room2)->AddExit(oppositeD(target), getRoom(room1));
                } else {
                    std::cout << "Invalid input. No connection made." << std::endl;
                }
        }

        std::string oppositeD(std::string direction) {
            if (direction == "north") return "south";
            if (direction == "south") return "north";
            if (direction == "east") return "west";
            if (direction == "west") return "east";
            return "";
        }

        void loadMapFile(const std::string& file) {
            std::string fileName = file;
            std::ifstream inputFile(fileName);

            if (!inputFile.is_open()) {
                //error opening file
                return;
            }
            
            std::string currentLine;

            while (std::getline(inputFile,currentLine)) {
                
                if (!currentLine.empty()) {

                    int findPos1 = currentLine.find("|");
                    int findPos2 = currentLine.find("|",findPos1+1);
                    int findPos3 = currentLine.find("|",findPos2+1);
                    std::string select = currentLine.substr(0,findPos1);

                    if (select == "Room") {
                        std::string name = currentLine.substr(findPos1+1, findPos2-findPos1-1);
                        std::string desc = currentLine.substr(findPos2+1);
                        Room* newRoom = new Room(desc);
                        this->AddRoom(name, newRoom);
                    } else if (select == "Item") {
                        std::string room = currentLine.substr(findPos1+1, findPos2-findPos1-1);
                        findPos1 = findPos2;
                        findPos2 = currentLine.find("|",findPos1+1);
                        std::string name = currentLine.substr(findPos1+1, findPos2-findPos1-1);
                        std::string desc = currentLine.substr(findPos2+1);
                        std::string sval = currentLine.substr(findPos3+1);
                        const int random = rand() % 100;
                        Item* newItem = new Item(name, desc, random);
                        this->getRoom(room)->AddItem(*newItem);
                    } else if (select == "Connection") {
                        std::string room1 = currentLine.substr(findPos1+1,findPos2-findPos1-1);
                        findPos1 = findPos2;
                        findPos2 = currentLine.find("|", findPos1+1);
                        std::string room2 = currentLine.substr(findPos1+1,findPos2-findPos1-1);
                        std::string cardinal = currentLine.substr(findPos2+1);
                        connectRooms(room1,room2,cardinal);
                    } else {
                        //error
                    }

                }

            }
            inputFile.close();
        }

};

#endif