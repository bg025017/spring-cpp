#include "httplib.h"
//this library has been optained for github https://github.com/kleinschrader/kleinsHTTP
//it has similar syntax to express.js (a library I have experience with)
using namespace httplib;

//endpoints requiered for the game
// 1. assign player 1 and 2
// 2. tell the server where each player is
// 3. once both players tell the server where they are, the server will tell them if they are in the
// same room, if so the server will use their scores to determine the winner


int main(void) {
  Server svr;

  svr.Get("/hi", [](const Request & /*req*/, Response &res) {
    res.set_content("Hello World!", "text/plain");
  });

  //player assignments, whoever requests first is player 1
    bool player1 = false;
    bool canStart = false;
    //storing player locations

    std::string player1Location;
    std::string player2Location;

    std::string playerHasWonMessage = "";




svr.Get("/playerAssign", [&player1, &canStart](const Request & /*req*/, Response &res) {
    std::cout << "player assign called" << std::endl;
    if (player1 == false) {
        player1 = true;
        res.set_content("p1", "text/plain");
        std::cout << "player 1 assigned" << std::endl;
    }
    else {
        canStart = true;
        res.set_content("p2", "text/plain");
        std::cout << "player 2 assigned" << std::endl;
    }
});

//allow start if both players are assigned
svr.Get("/start", [&canStart](const Request & /*req*/, Response &res) {
    std::cout << "start called" << std::endl;
    if (canStart == true) {
        res.set_content("start", "text/plain");
        std::cout << "game started" << std::endl;
    }
    else {
        std::cout << "waiting for other player to join" << std::endl;
        res.set_content("wait", "text/plain");
    }
});

svr.Get("/playerLocation", [&player1Location, &player2Location, &player1, &canStart, &playerHasWonMessage](const Request &req, Response &res) {

    std::string player1Score;
    std::string player2Score;

    if(playerHasWonMessage != "") {
        res.set_content(playerHasWonMessage, "text/plain");
        return;
    }

    if (req.has_param("score")) {
        if (req.get_param_value("player") == "p1") {
            player1Score = req.get_param_value("score");
            player1Location = req.get_param_value("location");
        }
        else {
            player2Score = req.get_param_value("score");
            player2Location = req.get_param_value("location");
        }
    }

    std::cout << "player 1 location: " << player1Location << std::endl;
    std::cout << "player 2 location: " << player2Location << std::endl;


    if (player1Location == player2Location) {
        std::cout << "================== players are in the same room =================" << std::endl;

        float a;
        sscanf(player1Score.c_str(), "%f", &a);
        float b;
        sscanf(player2Score.c_str(), "%f", &b);
        //Ik what I just wrote is an absolute disaster, as is the rest of my code but for some reason:
        // int player1ScoreInt = std::stoi(player1Score);
        // int player2ScoreInt = std::stoi(player2Score);
        // just skipped the rest of this file, I have no clue why

        if (a > b) {
            playerHasWonMessage = "player 1 has won";
            res.set_content("1win", "text/plain");
        }
        else {
            playerHasWonMessage = "player 2 has won";
            res.set_content("2win", "text/plain");
        }
        std::cout << "game over, resetting" << std::endl;
    }
    else {
        res.set_content("skip", "text/plain");
    }

});



svr.listen("0.0.0.0", 8080);
}