#include <string>
#include "clientCom.h"

#include <chrono>

class Timer {
private:
    std::chrono::time_point<std::chrono::steady_clock> start_time;

public:
    Timer() {
        reset();
    }

    void reset() {
        start_time = std::chrono::steady_clock::now();
    }

    bool hasElapsedSecs(int seconds) {
        auto elapsed_time = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - start_time);
        return elapsed_time.count() >= seconds;
    }
};



int main() {
    ClientCom client;
    client.login();
    bool continueGame = false;
    if(client.getPlayer() == "p2") {
        continueGame = true;
    }
    while (continueGame == false) {
        std::cout << "waiting for other player to join, press enter to retry" << std::endl;
        continueGame = client.requestStart();
        if(continueGame == true) {
            break;
        }
        std::cin.ignore();
    }
    std::cout << "game started" << std::endl;

    std::string rooms[10] = {"Kitchen", "Living Room", "Bedroom", "Bathroom", "Garage"};

    for(const auto& room : rooms) {
        std::cout << "You are now in the " << room << std::endl;
        std::string res = client.sendLocation(room, 0);
        std::cout << "server res: " + res << std::endl;
        Timer timer;
        bool canMove = false;
        while (timer.hasElapsedSecs(5) == false) {
            std::cout << "waiting for other player to move, press enter to retry" << std::endl;
            std::string res = client.sendLocation(room, 0);
            std::cout << "server res: " + res << std::endl;
            if(res == "start") {
                canMove = true;
                break;
            }
            std::cin.ignore();
        }
    }
    

    return 0;
}