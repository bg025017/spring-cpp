#include <iostream>
#include <vector>
#include <map>


#include "item.h"
#include "room.h"
#include "area.h"
#include "playerCharacter.h"
#include "clientCom.h"

// Include the Room and Item classes (assuming they are defined in separate files, which they should be!)
int main()
{
    Area gameWorld;
    gameWorld.loadMapFile("rooms.txt");
    // Create Items
    // Item key("Key", "A shiny key that looks important.");
    // Item sword("Sword", "A sharp sword with a golden hilt.");
    ClientCom client;
    client.login();
    bool continueGame = false;
    if(client.getPlayer() == "p2") {
        continueGame = true;
    }
    while (continueGame == false) {
        std::cout << "waiting for other player to join, press enter to retry" << std::endl;
        continueGame = client.requestStart();
        if(continueGame == true) {
            break;
        }
        std::cin.ignore();
    }
    std::cout << "game started" << std::endl;



    Player player("Alice", 100);
    player.SetLocation(gameWorld.getRoom(client.getPlayer() == "p1" ? "startingroom" : "dungeon"));
    while (true)
    {
        std::cout << "Current Location: " << player.GetLocation() -> GetDescription() << std::endl;
        std::cout << "Current health: " << player.getHealth() << std::endl;
        std::cout << "Items in the room:" << std::endl;
        for (const Item &item : player.GetLocation()->GetItems())
        {
            std::cout << "- " << item.GetName() << ": " << item.GetDescription() << std::endl;
        }
        std::string res = client.sendLocation(player.GetLocation()->GetDescription(), player.getHealth());
        std::cout << "======================= server res: " + res << std::endl;
        std::cout << "Options: ";
        std::cout << "1. Look around | ";
        std::cout << "2. Interact with an item | ";
        std::cout << "3. Move to another room | ";
        std::cout << "4. Quit" << std::endl;
        int choice;
        std::cin >> choice;
        if (choice == 1)
        {
            // Player looks around (no action required)
            std::cout << "You look around the room." << std::endl;
        }
        else if (choice == 2)
        {
            // Player interacts with an item in the room
            std::cout << "Enter the name of the item you want to interact with : ";
            std::string itemName;
            std::cin >> itemName;
            for (Item &item : player.GetLocation()->GetItems())
            {
                if (item.GetName() == itemName)
                {
                    item.Interact();
                    std::cout << "You interact with the " << item.GetName() << "." << std::endl;
                    std::cout << "You gained " << item.Interact() << " health." << std::endl;
                    break;
                }
            }
        }
        else if (choice == 3)
        {
            // Player moves to another room
            std::cout << "Enter the direction (e.g., north, south): ";
            std::string direction;
            std::cin >> direction;
            //Room *nextRoom = player.GetLocation()->GetExit(direction);
            if (/*nextRoom != nullptr*/ player.GetLocation()->isValidExit(direction))
            {
                Room *nextRoom = player.GetLocation()->GetExit(direction);
                player.SetLocation(nextRoom);
                std::cout << "You move to the next room." << std::endl;
            }
            else
            {
                std::cout << "You can't go that way." << std::endl;
            }
        }
        else if (choice == 4)
        {
            // Quit the game
            std::cout << "Goodbye!" << std::endl;
            break;
        }
        else
        {
            std::cout << "Invalid choice. Try again." << std::endl;
            break;
        }
    }
    return 0;
}