#include <iostream>
#include <string>
#pragma once
class Item
{
private:
    std::string name;
    std::string description;
    int value;

public:
    Item(const std::string &n, const std::string& desc, const int &v) {
        name = n;
        description = desc;
        value = v;
    }

    int Interact() {
        //code for interacting with an item
        return value;
    }

    std::string GetDescription() const {
        return description;
    }

    std::string GetName() const {
        return name;
    }

    bool operator==(const Item& other) const {
    // compare the items based on their properties
    return this->name == other.name && this->description == other.description;
}
};
// Example usage:
//Item key("Key", "A shiny key that looks important.");